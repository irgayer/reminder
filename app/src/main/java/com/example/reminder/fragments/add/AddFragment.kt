package com.example.reminder.fragments.add
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.reminder.R
import com.example.reminder.data.entities.Reminder
import com.example.reminder.data.viewmodels.ReminderViewModel
import com.example.reminder.services.ReminderBroadcast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import java.util.*

class AddFragment : Fragment() {
    private lateinit var mReminderViewModel : ReminderViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add, container, false)

        mReminderViewModel = ViewModelProvider(this).get(ReminderViewModel::class.java)

        view.btnAdd.setOnClickListener{
            insertDataToDatabase()
        }

        return view
    }

    private fun insertDataToDatabase() {
        //get all data from fields
        var name =  etName.text.toString()
        var description = etDescription.text.toString()
        var hour: String = timePicker.hour.toString()
        if (timePicker.hour < 10)
        {
            hour = "0${timePicker.hour}"
        }
        var min: String = timePicker.minute.toString()
        if (timePicker.minute < 10)
        {
            min = "0${timePicker.minute}"
        }

        var time = "${hour}:${min}"
        var isOn = cbIsOn.isChecked

        //checking validation name is not empty
        if (inputCheck(name)) {
            //creating data object
            var reminder = Reminder(0, name, isOn.toString(), description, time)

            //put into database
            mReminderViewModel.addReminder(reminder).observe(viewLifecycleOwner, Observer {
                ret ->
                reminder.uid = ret.toInt()
                //if "Is On" Checked
                if (cbIsOn.isChecked) {
                    //create channel and notification
                    createNotificationChannel(reminder)
                    createAlarm(reminder)
                }

                Toast.makeText(requireContext(), "Successfully added!", Toast.LENGTH_LONG).show()
            })
        } else {
            Toast.makeText(requireContext(), "Please fill out fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(name: String) : Boolean {
        return !(TextUtils.isEmpty(name))
    }

    //Create notification
    private fun createAlarm(reminder: Reminder) {
        var intent = Intent(activity, ReminderBroadcast::class.java)
        //serializing data to json because we need to get name and description later in ReminderBroadcast.kt
        var gson = Gson()
        intent.setAction(gson.toJson(reminder))

        //Creating notification
        var pIntent = PendingIntent.getBroadcast(activity, 0, intent, 0)

        var alarmManager = activity?.getSystemService(ALARM_SERVICE) as AlarmManager

        Log.d("createAlarm", reminder.name)

        //Getting exact time
        var calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
        calendar.set(Calendar.MINUTE, timePicker.minute)
        calendar.set(Calendar.SECOND, 30)
        calendar.set(Calendar.MILLISECOND, 0)


        //Setting notification on time
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pIntent)
    }

    //Create channel where we will get notification
    private fun createNotificationChannel(reminder: Reminder) {
        var name : CharSequence = "ReminderChannel for ${reminder.name}"
        var description = "Channel for Reminder Alarms"
        var importance = NotificationManager.IMPORTANCE_HIGH

        Log.d("createNotificationChannel", reminder.name)
        var channel = NotificationChannel(reminder.uid.toString(), name, importance)
        channel.description = description

        var notificationManager = activity?.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(channel)
    }

}