package com.example.reminder.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.reminder.R
import com.example.reminder.data.entities.Reminder
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.item_reminder.view.*

class ListAdapter : RecyclerView.Adapter<ListAdapter.RViewHolder>() {
    private var reminderList = emptyList<Reminder>()

    class RViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RViewHolder {
        return RViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_reminder, parent, false))
    }

    override fun getItemCount(): Int {
        return reminderList.size
    }

    //When clicking on single reminder
    override fun onBindViewHolder(holder: RViewHolder, position: Int) {
        var currentItem = reminderList[position]
        holder.itemView.tvReminderName.text = currentItem.name
        holder.itemView.tvReminderTime.text = currentItem.time
        //holder.itemView.cbAlarm.isChecked = currentItem.isOn.toBoolean()

        //open UpdateFragment
        holder.itemView.rowLayout.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    //Initialize data
    fun setData(reminder: List<Reminder>)
    {
        this.reminderList = reminder
        notifyDataSetChanged()
    }
}