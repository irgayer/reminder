package com.example.reminder.fragments.update

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.reminder.R
import com.example.reminder.R.menu
import com.example.reminder.data.entities.Reminder
import com.example.reminder.data.viewmodels.ReminderViewModel
import com.example.reminder.services.ReminderBroadcast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_update.*
import kotlinx.android.synthetic.main.fragment_update.view.*
import java.util.*


class UpdateFragment : Fragment() {
    private val args by navArgs<UpdateFragmentArgs>()

    private lateinit var mReminderViewModel: ReminderViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_update, container, false)

        mReminderViewModel = ViewModelProvider(this).get(ReminderViewModel::class.java)

        //Set inputs to data of select reminder
        view.etUpdName.setText(args.currentReminder.name)
        view.etUpdDescription.setText(args.currentReminder.description)
        view.cbUpdIsOn.isChecked = args.currentReminder.isOn.toBoolean()
        view.timeUpdPicker.hour = args.currentReminder.time.toString().substring(0, 2).toInt()
        view.timeUpdPicker.minute = args.currentReminder.time.toString().substring(3, 5).toInt()

        view.btnUpdate.setOnClickListener {
            updateItem()
        }

        setHasOptionsMenu(true)

        return view
    }

    private fun updateItem() {
        //Getting inputs
        var name =  etUpdName.text.toString()
        var description = etUpdDescription.text.toString()
        var hour: String = timeUpdPicker.hour.toString()
        if (timeUpdPicker.hour < 10)
        {
            hour = "0${timeUpdPicker.hour}"
        }
        var min: String = timeUpdPicker.minute.toString()
        if (timeUpdPicker.minute < 10)
        {
            min = "0${timeUpdPicker.minute}"
        }

        var time = "${hour}:${min}"
        var isOn = cbUpdIsOn.isChecked

        //input is valid?
        if (inputCheck(name)) {
            //Update in database
            val updatedReminder = Reminder(args.currentReminder.uid, name, isOn.toString(), description, time)

            mReminderViewModel.updateReminder(updatedReminder)
            Toast.makeText(requireContext(), "Successfully changed!", Toast.LENGTH_LONG).show()
            //if "Is On" checked
            if (cbUpdIsOn.isChecked)
            {
                createNotificationChannel(updatedReminder)
                createAlarm(updatedReminder)
            }

            //return to listFragment
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        } else {
            Toast.makeText(requireContext(), "Please fill out fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(name: String) : Boolean {
        return !(TextUtils.isEmpty(name))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.reminder_menu, menu)
    }

    //When we click on "Trashcan" button delete Notification
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete) {
            deleteReminder()
        }
        return super.onOptionsItemSelected(item)
    }

    //Delete notification
    private fun deleteReminder() {
        //ask user if they sure about this
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes") { _, _ ->
            //delete from database
            mReminderViewModel.deleteReminder(args.currentReminder)
            Toast.makeText(requireContext(), "Successfully removed: ${args.currentReminder.name}!", Toast.LENGTH_LONG).show()

            //delete notification
            deleteAlarm(args.currentReminder)

            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }
        builder.setNegativeButton("No") { _, _ ->

        }
        builder.setTitle("Delete ${args.currentReminder.name}")
        builder.setMessage("Are you sure you want to delete ${args.currentReminder.name}?")
        builder.create().show()
    }

    //Delete notification
    private fun deleteAlarm(reminder: Reminder) {
        //as creating, but canceling
        var am = activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        var intent = Intent(activity, ReminderBroadcast::class.java)
        var gson = Gson()
        intent.setAction(gson.toJson(reminder))
        var pIntent = PendingIntent.getBroadcast(activity, 0, intent, 0)

        am.cancel(pIntent)
    }

    //Create notification
    private fun createAlarm(reminder: Reminder) {
        var intent = Intent(activity, ReminderBroadcast::class.java)
        //serializing data to json because we need to get name and description later in ReminderBroadcast.kt
        var gson = Gson()
        intent.setAction(gson.toJson(reminder))

        //Creating notification
        var pIntent = PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        var alarmManager = activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        Log.d("createAlarm", reminder.name)

        //Getting exact time
        var calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, timeUpdPicker.hour)
        calendar.set(Calendar.MINUTE, timeUpdPicker.minute)
        calendar.set(Calendar.SECOND, 30)
        calendar.set(Calendar.MILLISECOND, 0)

        //Setting notification on time
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pIntent)
    }

    //Create channel where we will get notification
    private fun createNotificationChannel(reminder: Reminder) {
        var name : CharSequence = "ReminderChannel for ${reminder.name}"
        var description = "Channel for Reminder Alarms"
        var importance = NotificationManager.IMPORTANCE_HIGH

        Log.d("createNotificationChannel", reminder.name)
        var channel = NotificationChannel(reminder.uid.toString(), name, importance)
        channel.description = description

        var notificationManager = activity?.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(channel)
    }
}