package com.example.reminder.services

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.reminder.R
import com.example.reminder.data.entities.Reminder
import com.google.gson.Gson

class ReminderBroadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        //getting data and setting notification information and view
        var serializable = intent?.action;
        var gson = Gson()
        var reminder: Reminder = gson.fromJson(serializable, Reminder::class.java)
        var builder = NotificationCompat.Builder(context!!, reminder.uid.toString() ?: "undefined")
        if (serializable != null) {
            Log.d("onReceive", serializable)
        }
        builder.setSmallIcon(R.drawable.ic_checked)
            .setContentTitle(reminder.name)
            .setContentText(reminder.description)
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        var manager = NotificationManagerCompat.from(context)

        manager.notify(reminder.uid, builder.build())
    }
}