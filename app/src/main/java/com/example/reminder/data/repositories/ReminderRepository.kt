package com.example.reminder.data.repositories

import androidx.lifecycle.LiveData
import com.example.reminder.data.daos.ReminderDao
import com.example.reminder.data.entities.Reminder

//Repository pattern
class ReminderRepository(private val reminderDao: ReminderDao) {
    val getAll : LiveData<List<Reminder>> = reminderDao.getAll()

    fun addReminder(reminder: Reminder) : Long {
        return reminderDao.insert(reminder)
    }

    suspend fun updateReminder(reminder: Reminder) {
        return reminderDao.update(reminder)
    }

    suspend fun deleteReminder (reminder: Reminder) {
        reminderDao.delete(reminder)
    }
}