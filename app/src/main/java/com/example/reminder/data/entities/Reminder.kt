package com.example.reminder.data.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

//Data Model
@Parcelize
@Entity
data class Reminder(
    @PrimaryKey(autoGenerate = true)
    var uid: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "is_on")
    var isOn: String = "false",

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "time")
    val time: String
) : Parcelable