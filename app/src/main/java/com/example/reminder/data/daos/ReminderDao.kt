package com.example.reminder.data.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.reminder.data.entities.Reminder

//Data Access Object
@Dao
interface ReminderDao {
    @Query("SELECT * from reminder")
    fun getAll() : LiveData<List<Reminder>>

    @Insert
    fun insert(reminder: Reminder) : Long

    @Update
    suspend fun update(reminder: Reminder)

    @Delete
    fun delete(reminder: Reminder)
}