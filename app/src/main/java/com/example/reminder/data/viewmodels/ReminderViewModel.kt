package com.example.reminder.data.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope

import com.example.reminder.data.entities.Reminder
import com.example.reminder.data.repositories.ReminderRepository
import com.example.reminder.data.db.ReminderDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//Realization
class ReminderViewModel(application: Application) : AndroidViewModel(application) {
    val getAll : LiveData<List<Reminder>>
    private val repository: ReminderRepository
    init {
        val reminderDao = ReminderDatabase.getDatabase(application).reminderDao()
        repository = ReminderRepository(reminderDao)
        getAll = repository.getAll
    }

    fun addReminder(reminder: Reminder) : MutableLiveData<Long>
    {
        val result = MutableLiveData<Long>()
        viewModelScope.launch(Dispatchers.IO) {
            val ret = repository.addReminder(reminder)
            result.postValue(ret)
        }
        return result
    }

    fun updateReminder(reminder: Reminder) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateReminder(reminder)
        }
    }

    fun deleteReminder(reminder: Reminder) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteReminder(reminder)
        }
    }
}